import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.slynklev.axal.Main;
import com.slynklev.axal.aXal;
import com.slynklev.axal.models.Block;
import com.slynklev.axal.models.Level;


public class MainEditorWindow{

	private JFrame frame;
	private JFileChooser dlgSave;
	private JFileChooser dlgTile;
	private NewMapDialog dlgNewMap;
	private JMenuItem mntmSelection;
	private JMenuItem mntmClickPlace;
	private JMenuItem mntmBoxFill;
	private JMenuItem mntmDraw;
	private JMenuItem mntmSetSpawn;
	private TilemapRenderer tmr;
	private File levelFile = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainEditorWindow window = new MainEditorWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainEditorWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		dlgNewMap = new NewMapDialog();
		dlgNewMap.setModalityType(ModalityType.APPLICATION_MODAL);
		
		dlgSave = new JFileChooser();
		dlgSave.setAcceptAllFileFilterUsed(false);
		dlgSave.addChoosableFileFilter(new FileNameExtensionFilter("*.bin", "bin"));
		dlgSave.setFileFilter(dlgSave.getChoosableFileFilters()[0]);
		File defaultFile = new File(new File(".").getAbsoluteFile().getParentFile().getParentFile().getParentFile(), "aXal-android/assets/levels/level.bin");
		dlgSave.setSelectedFile(defaultFile);
		
		dlgTile = new JFileChooser();
		dlgTile.setAcceptAllFileFilterUsed(false);
		dlgTile.addChoosableFileFilter(new FileNameExtensionFilter("*.pack", "pack"));
		dlgTile.setFileFilter(dlgTile.getChoosableFileFilters()[0]);
		File defaultTile = new File(new File(".").getAbsoluteFile().getParentFile().getParentFile().getParentFile(), "aXal-android/assets/images/packs/tiles.pack");
		dlgTile.setSelectedFile(defaultTile);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mnFile.add(mntmNew);
		mntmNew.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newMap();
			}
        });
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mnFile.add(mntmOpen);
		mntmOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				open();
			}
        });
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
		mnFile.add(mntmSave);
		mntmSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Save");
				save();
			}
        });
		
		JMenuItem mntmSaveAs = new JMenuItem("Save as...");
		mnFile.add(mntmSaveAs);
		mnFile.addSeparator();
		mntmSaveAs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveAs();
			}
        });
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, KeyEvent.CTRL_DOWN_MASK));
		mnFile.add(mntmExit);
		mntmExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				exit();
			}
        });
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		JMenuItem mntmCut = new JMenuItem("Cut");
		mntmCut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_DOWN_MASK));
		mnEdit.add(mntmCut);
		mntmCut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cut();
			}
        });
		
		JMenuItem mntmCopy = new JMenuItem("Copy");
		mntmCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
		mnEdit.add(mntmCopy);
		mntmCopy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				copy();
			}
        });
		
		JMenuItem mntmPaste = new JMenuItem("Paste");
		mntmPaste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_DOWN_MASK));
		mnEdit.add(mntmPaste);
		mntmPaste.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				paste();
			}
        });
		
		mnEdit.addSeparator();
		
		JMenu mnCursorMode = new JMenu("Cursor Mode");
		mnEdit.add(mnCursorMode);
		
		mntmSelection = new JMenuItem("Selection");
		mntmSelection.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, KeyEvent.SHIFT_DOWN_MASK));
		mnCursorMode.add(mntmSelection);
		mntmSelection.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setSelectionMode();
			}
        });
		
		mntmClickPlace = new JMenuItem("Click Place");
		mntmClickPlace.setBackground(Color.RED);
		mntmClickPlace.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, KeyEvent.SHIFT_DOWN_MASK));
		mnCursorMode.add(mntmClickPlace);
		mntmClickPlace.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setClickPlace();
			}
        });
		
		mntmBoxFill = new JMenuItem("Box Fill");
		mntmBoxFill.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, KeyEvent.SHIFT_DOWN_MASK));
		mnCursorMode.add(mntmBoxFill);
		mntmBoxFill.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setBoxFill();
			}
        });
		
		mntmDraw = new JMenuItem("Draw");
		mntmDraw.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, KeyEvent.SHIFT_DOWN_MASK));
		mnCursorMode.add(mntmDraw);
		mntmDraw.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setDraw();
			}
        });
		
		mntmSetSpawn = new JMenuItem("Set Spawn");
		menuBar.add(mntmSetSpawn);
		mntmSetSpawn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setSpawn();
			}
        });
		
		JMenuItem mntmPlay = new JMenuItem("Play!");
		mntmPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playTest();
			}
		});
		menuBar.add(mntmPlay);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.85f);
		frame.getContentPane().add(splitPane);
		
		JScrollPane scrRenderer = new JScrollPane();
		scrRenderer.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrRenderer.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		splitPane.setLeftComponent(scrRenderer);
		
		tmr = new TilemapRenderer();
		scrRenderer.setViewportView(tmr);
		
		JScrollPane scrSelecter = new JScrollPane();
		scrSelecter.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrSelecter.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		splitPane.setRightComponent(scrSelecter);
		
		TileSelecter ts = new TileSelecter();
		scrSelecter.setViewportView(ts);
	}
	
	private void newMap() {
		final int ret = dlgTile.showOpenDialog(frame);
		
		if(ret == JFileChooser.APPROVE_OPTION) {
			final File tilePack = dlgTile.getSelectedFile();
			String name = tilePack.getName();
			name = name.substring(0, name.indexOf('.'));
			
			final File use = new File(tilePack.getParentFile(), name);
			
			try {
				TileDriver.setTileset(new Tileset(use.getAbsolutePath()));
				dlgNewMap.setVisible(true);
				if(dlgNewMap.hitOk()) {
					final int[] r = dlgNewMap.getDimensions();
					System.out.println("Ret: " + r[0] + "x" + r[1]);
					
					final Block[][] blocks = new Block[r[0]][r[1]];
					
					for(int x = 0; x < blocks.length; x++)
						for(int y = 0; y < blocks[0].length; y++)
							blocks[x][y] = null;
					
					TileDriver.setBlocks(blocks);
					
					tmr.setBounds(tmr.getX(), tmr.getY(), 1, 1);
					
					levelFile = null;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	private void open() {
		final int ret = dlgTile.showOpenDialog(frame);
		
		if(ret == JFileChooser.APPROVE_OPTION) {
			final File tilePack = dlgTile.getSelectedFile();
			String name = tilePack.getName();
			name = name.substring(0, name.indexOf('.'));
			
			final File use = new File(tilePack.getParentFile(), name);
			
			try {
				TileDriver.setTileset(new Tileset(use.getAbsolutePath()));
				
				final int ret2 = dlgSave.showOpenDialog(frame);
				
				if(ret2 == JFileChooser.APPROVE_OPTION) {
					levelFile = dlgSave.getSelectedFile();
					
					System.out.println(levelFile.toString());
					
					Input input = null;
					try {
						input = new Input(new FileInputStream(levelFile));
						final Level level =  new Kryo().readObject(input, Level.class);
						TileDriver.setBlocks(level.getBlocks());
						TileDriver.setSpawnPoint(level.getSpawnLocation());
					} catch(Exception e) {
						e.printStackTrace();
					} finally {
						input.close();
					}
					
					tmr.setBounds(tmr.getX(), tmr.getY(), 1, 1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	private void save(){
		if(levelFile == null)
			saveAs();
		else
			writeLevel();
	}
	
	private void saveAs(){
		final int ret = dlgSave.showSaveDialog(frame);
		
		if(ret == JFileChooser.APPROVE_OPTION) {
			levelFile = dlgSave.getSelectedFile();
			writeLevel();
		}
	}
	
	private void exit(){
		System.exit(0);
	}
	
	private void cut() {
		
	}
	
	private void copy() {
		
	}
	
	private void paste() {
		
	}
	
	private void setSelectionMode() {
		TileDriver.setMode(TileDriver.Mode.SELECTION);
		
		mntmClickPlace.setBackground(Color.WHITE);
		mntmClickPlace.repaint();
		
		mntmBoxFill.setBackground(Color.WHITE);
		mntmBoxFill.repaint();
		
		mntmDraw.setBackground(Color.WHITE);
		mntmDraw.repaint();
		
		mntmSelection.setBackground(Color.RED);
		mntmSelection.repaint();
		
		mntmSetSpawn.setEnabled(true);
	}
	
	private void setClickPlace() {
		TileDriver.setMode(TileDriver.Mode.CLICK_PLACE);
		
		mntmClickPlace.setBackground(Color.RED);
		mntmClickPlace.repaint();
		
		mntmBoxFill.setBackground(Color.WHITE);
		mntmBoxFill.repaint();
		
		mntmDraw.setBackground(Color.WHITE);
		mntmDraw.repaint();
		
		mntmSelection.setBackground(Color.WHITE);
		mntmSelection.repaint();
		
		mntmSetSpawn.setEnabled(true);
	}
	
	private void setBoxFill() {
		TileDriver.setMode(TileDriver.Mode.BOX_FILL);
		
		mntmClickPlace.setBackground(Color.WHITE);
		mntmClickPlace.repaint();
		
		mntmBoxFill.setBackground(Color.RED);
		mntmBoxFill.repaint();
		
		mntmDraw.setBackground(Color.WHITE);
		mntmDraw.repaint();
		
		mntmSelection.setBackground(Color.WHITE);
		mntmSelection.repaint();
		
		mntmSetSpawn.setEnabled(true);
	}
	
	private void setDraw() {
		TileDriver.setMode(TileDriver.Mode.DRAW);
		
		mntmClickPlace.setBackground(Color.WHITE);
		mntmClickPlace.repaint();
		
		mntmBoxFill.setBackground(Color.WHITE);
		mntmBoxFill.repaint();
		
		mntmDraw.setBackground(Color.RED);
		mntmDraw.repaint();
		
		mntmSelection.setBackground(Color.WHITE);
		mntmSelection.repaint();
		
		mntmSetSpawn.setEnabled(true);
	}
	
	private void setSpawn() {
		mntmSetSpawn.setEnabled(false);
		TileDriver.setMode(TileDriver.Mode.SET_SPAWN);
		TileDriver.setSpawnSetListener(new Callback(){

			@Override
			public void call() {
				mntmSetSpawn.setEnabled(true);
				mntmClickPlace.doClick();
			}
			
		});
	}
	
	private void playTest() {
		new Thread(new Runnable() {
	        public void run() {
	        	try {
		        	
		        	String javaHome = System.getProperty("java.home");
		            String javaBin = javaHome +
		                    File.separator + "bin" +
		                    File.separator + "java";
		            String classpath = System.getProperty("java.class.path");
		            String className = Main.class.getCanonicalName();
	
		            ProcessBuilder builder = new ProcessBuilder(
		                    javaBin, "-cp", classpath, className, "-l", "level1.bin");
	
		            Process process = builder.start();
		            process.waitFor();
		            
		           final  BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		            
		            String s = null;
		            while ((s = stdInput.readLine()) != null) {
		                System.out.println(s);
		            }
	        	} catch(Throwable t) {
	        		t.printStackTrace();
	        	}
	        }
	    }).start();
	}
	
	private void writeLevel() {
		final Level level = new Level();
		level.setBlocks(TileDriver.getBlocks());
		level.setSpawnLocation(TileDriver.getSpawnLocation());
		
		if(levelFile.exists())
			levelFile.delete();
		
		Kryo kryo = new Kryo();
		Output output = null;
		try{
			output = new Output(new FileOutputStream(levelFile));
			kryo.writeObject(output, level);
		} catch(final Exception e) {
			e.printStackTrace();
		} finally {
			output.close();
		}
	}
}
