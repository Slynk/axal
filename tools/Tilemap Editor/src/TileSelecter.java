import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;


public class TileSelecter extends JPanel implements MouseInputListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final int BORDER = 4;
	
	public TileSelecter() {
		this.setBackground(Color.BLACK);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		
		TileDriver.setTilesetListener(REDRAW);
	}
	
	@Override
    protected void paintComponent(Graphics g) {
		final Tileset tileset = TileDriver.getTileset();
		
		if(tileset != null) {
			g.setColor(this.getBackground());
	        g.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
	        g.setColor(this.getForeground());
			
			final int tilesWide = this.getWidth() / (tileset.getTileSize() + (BORDER * 2));
			
			final Image[] tiles = tileset.getTiles();
			
			this.setPreferredSize(new Dimension(this.getWidth(), ((tiles.length/tilesWide) + 1) * (tileset.getTileSize() + BORDER + BORDER) ));
			
			for(int i = 1; i < tiles.length +1; i++) {
				g.drawImage(tiles[i -1], 
						(i * tileset.getTileSize()) + (BORDER * (1+i)), 
						((i / tilesWide) * tileset.getTileSize()) + (BORDER * ((i / tilesWide) + 1)) , null);
			}
			
			final int selectedTile = TileDriver.getSelectedTile() + 1;
			g.setColor(Color.WHITE);
			g.drawRect((selectedTile * tileset.getTileSize()) + (BORDER * (selectedTile)) + (BORDER/2),
					((selectedTile / tilesWide) * tileset.getTileSize()) + (BORDER * ((selectedTile / tilesWide))) + (BORDER/2), 
					tileset.getTileSize() + (BORDER), 
					tileset.getTileSize() + (BORDER));
			g.setColor(this.getForeground());
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		final Tileset tileset = TileDriver.getTileset();
		
		final int tilesWide = this.getWidth() / (tileset.getTileSize() + (BORDER));
		final int col = e.getX() / (tileset.getTileSize() + (BORDER));
		final int row = e.getY() / (tileset.getTileSize() + (BORDER));
		
		TileDriver.setSelectedTile((row * tilesWide) + col - 1);
		
		System.out.println("Pressed: " + TileDriver.getSelectedTile());
		
		this.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		final Tileset tileset = TileDriver.getTileset();
		
		final int tilesWide = this.getWidth() / (tileset.getTileSize() + (BORDER * 2));
		final int col = e.getX() / (tileset.getTileSize() + (BORDER * 2));
		final int row = e.getY() / (tileset.getTileSize() + (BORDER * 2));
		
		TileDriver.setSelectedTile((row * tilesWide) + col - 1);
		
		System.out.println("Pressed: " + TileDriver.getSelectedTile());
		
		this.repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
	}
	
	private Callback REDRAW = new Callback() {

		@Override
		public void call() {
			TileSelecter.this.repaint();
		}
		
	};
}
