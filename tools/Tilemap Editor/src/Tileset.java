import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.TextureAtlasData;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.TextureAtlasData.Region;
import com.badlogic.gdx.utils.Array;


public class Tileset {
	private Image[] blocks;
	private int tSize = 1;
	
	public Tileset(final String path) throws Exception {
		
		final BufferedImage master = ImageIO.read(new File(path + ".png"));
		final FileHandle pack = new FileHandle(path + ".pack");
		final TextureAtlasData data = new TextureAtlasData(pack, pack.parent(), false);
		
		final Array<Region> regions = data.getRegions();
		blocks = new Image[regions.size];
		
		tSize = regions.get(0).width;
		
		for(int i = 0; i < blocks.length; i++) {
			final Region reg = regions.get(i);
			final int which = Integer.parseInt(reg.name);
				blocks[which] = master.getSubimage((int)reg.left, (int)reg.top, reg.width, reg.height);
		}
	}
	
	public int getTileSize() {
		return tSize;
	}
	
	public Image[] getTiles() {
		return blocks;
	}
}
