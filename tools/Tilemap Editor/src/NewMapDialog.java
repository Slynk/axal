import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class NewMapDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private boolean hitOk = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			NewMapDialog dialog = new NewMapDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public NewMapDialog() {
		setBounds(100, 100, 254, 152);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblWidth = new JLabel("Width:");
		lblWidth.setBounds(6, 42, 40, 16);
		contentPanel.add(lblWidth);
		
		textField = new JTextField();
		textField.setText("1");
		textField.setBounds(47, 36, 60, 28);
		contentPanel.add(textField);
		textField.setColumns(10);
		
		JLabel lblHeiht = new JLabel("Height:");
		lblHeiht.setBounds(119, 42, 46, 16);
		contentPanel.add(lblHeiht);
		
		textField_1 = new JTextField();
		textField_1.setText("1");
		textField_1.setBounds(177, 36, 67, 28);
		contentPanel.add(textField_1);
		textField_1.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						hitOk = true;
						NewMapDialog.this.setVisible(false);
					}
					
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						hitOk = false;
						NewMapDialog.this.setVisible(false);
					}
					
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		System.out.println("Component count: " + this.rootPane.getComponentCount());
	}
	
	public int[] getDimensions() {
		final int[] ret = new int[2];
		
		ret[0] = Integer.parseInt(textField.getText());
		ret[1] = Integer.parseInt(textField_1.getText());
		
		return ret;
	}
	
	public boolean hitOk() {
		return hitOk;
	}
}
