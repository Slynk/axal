import java.awt.event.MouseEvent;

import javax.swing.event.MouseInputListener;

import com.badlogic.gdx.math.Vector2;
import com.slynklev.axal.models.Block;


public class TileDriver {
	public enum Mode{
		SELECTION, CLICK_PLACE, BOX_FILL, DRAW, SET_SPAWN
	}
	
	private static Mode mode = Mode.CLICK_PLACE;
	
	private static int selectedTile = -1;
	
	private static Block[][] blocks = null;
	private static Vector2 spawnLocation = new Vector2(1,1);
	
	private static Tileset tileset = null;
	
	private static Callback map_edit_listener = null;
	private static Callback tileset_changed_listener = null;
	private static Callback spawn_set_listener = null;
	
	public static boolean checkMode(final Mode m) {
		return mode.equals(m);
	}

	public static Mode getMode() {
		return mode;
	}

	public static void setMode(Mode mode) {
		TileDriver.mode = mode;
	}

	public static int getSelectedTile() {
		return selectedTile;
	}

	public static void setSelectedTile(int selectedTile) {
		TileDriver.selectedTile = selectedTile;
	}

	public static Block[][] getBlocks() {
		return blocks;
	}

	public static void setBlocks(Block[][] blocks) {
		TileDriver.blocks = blocks;
		
		if(map_edit_listener != null)
			map_edit_listener.call();
	}

	public static Tileset getTileset() {
		return tileset;
	}

	public static void setTileset(Tileset tileset) {
		TileDriver.tileset = tileset;
		TileDriver.selectedTile = -1;
		
		if(tileset_changed_listener != null)
			tileset_changed_listener.call();
		
		if(map_edit_listener != null)
			map_edit_listener.call();
	}
	
	private static int[] mouseToTile(final MouseEvent e) {
		final int x = e.getX();
		final int y = e.getY();
		final int[] ret = new int[2];
		
		ret[0] = x / tileset.getTileSize();
		ret[1] = ((blocks[0].length * tileset.getTileSize()) - y) / tileset.getTileSize();
		
		return ret;
	}

	public static void setMapEditListener(Callback listener) {
		TileDriver.map_edit_listener = listener;
	}
	
	public static void setTilesetListener(Callback listener) {
		TileDriver.tileset_changed_listener = listener;
	}
	
	public static void setSpawnSetListener(Callback listener) {
		TileDriver.spawn_set_listener = listener;
	}
	
	private static void placeTile(MouseEvent e) {
		final int[] tile = mouseToTile(e);
		
		if(tile[0] < blocks.length && tile[1] < blocks[0].length) {
			if(TileDriver.getSelectedTile() < 0) {
				blocks[tile[0]][tile[1]] = null;
			} else {
				if(blocks[tile[0]][tile[1]] == null)
					blocks[tile[0]][tile[1]] = new Block(new Vector2(tile[0], tile[1]), selectedTile);
				
				blocks[tile[0]][tile[1]].setTexture(TileDriver.getSelectedTile());
			}
			
			if(map_edit_listener != null)
				map_edit_listener.call();
		}
	}

	public static MouseInputListener INPUT_LISTENER = new MouseInputListener() {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if(mode.equals(Mode.DRAW)) {
				placeTile(e);

			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if(mode.equals(TileDriver.Mode.CLICK_PLACE)) {
				placeTile(e);
			} else if(mode.equals(TileDriver.Mode.SET_SPAWN)) {
				System.out.println("Setting spawn...");
				final int ret[] = mouseToTile(e);
				
				spawnLocation.set((float)ret[0], (float)ret[1]);
				
				if(TileDriver.spawn_set_listener != null) {
					TileDriver.spawn_set_listener.call();
					TileDriver.spawn_set_listener = null;
				}
				
				if(map_edit_listener != null)
					map_edit_listener.call();
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			if(mode.equals(Mode.DRAW)){
				placeTile(e);
			}
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	};

	public static Vector2 getSpawnLocation() {
		return spawnLocation;
	}

	public static void setSpawnPoint(Vector2 spawnLocation) {
		TileDriver.spawnLocation = spawnLocation;
	}
}
