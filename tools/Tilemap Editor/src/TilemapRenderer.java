import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.badlogic.gdx.math.Vector2;
import com.slynklev.axal.models.Block;


public class TilemapRenderer extends JPanel{
	private int tilesX = 0;
	private int tilesY = 0;
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TilemapRenderer() {
		this.setBackground(Color.BLACK);
		this.setOpaque(true);
		this.addMouseListener(TileDriver.INPUT_LISTENER);
		this.addMouseMotionListener(TileDriver.INPUT_LISTENER);
		
		TileDriver.setMapEditListener(REDRAW);
		
//		final JScrollBar vBar = new JScrollBar(JScrollBar.VERTICAL);
//		
//		final JScrollBar hBar = new JScrollBar(JScrollBar.HORIZONTAL);
	}
	
	@Override
	public void setBounds(int x, int y, int width, int height){
		final Tileset tileset = TileDriver.getTileset();
		Block[][] blocks = TileDriver.getBlocks();
				
		if(tileset == null)
			super.setBounds(x, y, width, height);
		else {
			if(blocks == null){
				tilesX = width / tileset.getTileSize();
				tilesY = height / tileset.getTileSize();
			
			
				blocks = new Block[tilesX][tilesY];
				TileDriver.setBlocks(blocks);
			
				for (int col = 0; col < tilesX; col++) {
					for (int row = 0; row < tilesY; row++) {
						blocks[col][row] = null;
					}
				}
			} else {
				tilesX = blocks.length;
				tilesY = blocks[0].length;
			}
			
			super.setBounds(x, y, tilesX * tileset.getTileSize(), tilesY * tileset.getTileSize());
			
			this.setPreferredSize(new Dimension(tilesX * tileset.getTileSize(), tilesY * tileset.getTileSize()));
		}
	}
	
	public void resizeArray(final int x, final int y) {
		
	}
	
	@Override
    protected void paintComponent(Graphics g) {
		
		final Tileset tileset = TileDriver.getTileset();
		final Block[][] blocks = TileDriver.getBlocks();
		
		if(tileset != null && blocks != null) {
			final int height = blocks[0].length * tileset.getTileSize();
	        
	        g.setColor(this.getBackground());
	        g.fillRect(this.getParent().getX(), this.getParent().getX(), blocks.length * tileset.getTileSize(), height);
	        g.setColor(this.getForeground());

        	final Image[] tiles = tileset.getTiles();
        	
        	for(int x = 0; x < tilesX; x++) {
        		for(int y = 0; y < tilesY; y++) {
        			final Block block = blocks[x][y];
        			if(block != null){
        				g.drawImage(tiles[block.getTexture()], x * tileset.getTileSize(), (tileset.getTileSize() * (tilesY - 1)) - (y * tileset.getTileSize()), null);
        			}
        		}
        	}
        	
        	final Vector2 spawn = TileDriver.getSpawnLocation();
        	g.setColor(Color.GREEN);
        	g.drawLine(
        			(int)(spawn.x * tileset.getTileSize()), 
        			(int)(((tileset.getTileSize() * (tilesY)) - (spawn.y * tileset.getTileSize())) - (tileset.getTileSize()/2f)), 
        			(int)((spawn.x + 1) * tileset.getTileSize()),
        			(int)(((tileset.getTileSize() * (tilesY)) - (spawn.y * tileset.getTileSize())) - (tileset.getTileSize()/2f)));
        	
        	g.drawLine(
        			(int)((spawn.x * tileset.getTileSize())  + (tileset.getTileSize()/2f)), 
        			(int)(((tileset.getTileSize() * (tilesY)) - (spawn.y * tileset.getTileSize()))), 
        			(int)((spawn.x * tileset.getTileSize())  + (tileset.getTileSize()/2f)),
        			(int)((tileset.getTileSize() * (tilesY)) - ((spawn.y + 1) * tileset.getTileSize())));
        	
        	g.setColor(this.getForeground());
		} else {
			g.setColor(Color.WHITE);
	        g.fillRect(this.getParent().getX(), this.getParent().getX(), this.getWidth(), this.getHeight());
	        g.setColor(this.getForeground());
		}
    }
	
	private final Callback REDRAW = new Callback(){

		@Override
		public void call() {
			TilemapRenderer.this.repaint();
		}
		
	};
}
