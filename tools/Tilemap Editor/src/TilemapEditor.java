import java.io.File;
import java.io.FileOutputStream;

import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import com.slynklev.axal.models.Block;
import com.slynklev.axal.models.Level;
import com.slynklev.axal.models.Block.CollisionType;

/**
 * For now, just manually define a level and save it.
 * @author Slynk
 *
 */
public class TilemapEditor {
	public static void main(String[] args) {
		final Kryo kryo = new Kryo();
		Output output = null;
		final File directory = new File(new File (".").getAbsoluteFile().getParentFile().getParentFile().getParentFile(), "aXal-android/assets/levels/");
		
		/**
		 * Level 1
		 */
		Level level1 = new Level();
		int width = 15, height = 7;
		level1.setWidth(width);
		level1.setHeight(height);
		
		
		final Block[][] blocks = new Block[width][height];
		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				blocks[col][row] = null;
			}
		}
		
		for (int col = 0; col < width; col++) {
			int tile = 1;
			if(col == 0)
				tile = 0;
			else if(col == width -1)
				tile = 2;
			
			blocks[col][0] = new Block(new Vector2(col, 0), tile);
			if (col > 2) {
				blocks[col][6] = new Block(new Vector2(col, 6), tile);
				blocks[col][1] = new Block(new Vector2(col, 1), tile);
			}
		}
		
		blocks[1][3] = new Block(new Vector2(1,3), 1, CollisionType.TOP);
		
		blocks[14][2] = new Block(new Vector2(14, 2), 2);
		blocks[14][3] = new Block(new Vector2(14, 3), 2);
		blocks[14][4] = new Block(new Vector2(14, 4), 2);
		blocks[14][5] = new Block(new Vector2(14, 5), 2);

		blocks[6][3] = new Block(new Vector2(6, 3), 2);
		blocks[6][4] = new Block(new Vector2(6, 4), 2);
		blocks[6][5] = new Block(new Vector2(6, 5), 2);
		
		level1.setBlocks(blocks);
		
		final File outputFile = new File(directory, "level1.bin");
		if(outputFile.exists())
			outputFile.delete();
		
		try{
			output = new Output(new FileOutputStream(outputFile));
			kryo.writeObject(output, level1);
		} catch(final Exception e) {
			e.printStackTrace();
		} finally {
			output.close();
		}
		
		/********************************************/
		
	}
	
	
}
