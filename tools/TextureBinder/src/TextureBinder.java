import java.io.File;

import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

public class TextureBinder {
	public static void main(String[] args) {
		File parent = new File (".").getAbsoluteFile().getParentFile().getParentFile().getParentFile();
		File inputDirectory = new File(parent, "aXal-android/assets/images/");
		File outputDirectory = new File(parent, "aXal-android/assets/images/packs/");

		//TexturePacker2.process(new File(inputDirectory, "character").getAbsolutePath(), outputDirectory.getAbsolutePath(), "character.pack");
		TexturePacker2.process(new File(inputDirectory, "tiles").getAbsolutePath(), outputDirectory.getAbsolutePath(), "tiles.pack");
	}
}
