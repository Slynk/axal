package com.slynklev.axal.library.asset;

import com.badlogic.gdx.assets.AssetManager;

public class CustomAssetManager extends AssetManager {
	
	public <T> void addAsset(java.lang.String fileName, java.lang.Class<T> type, T asset) {
		super.addAsset(fileName, type, asset);
	}
}
