package com.slynklev.axal.library.asset;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Array;

public class FreeTypeFontLoader extends SynchronousAssetLoader<FreeTypeFontGenerator, AssetLoaderParameters<FreeTypeFontGenerator>> {

	public FreeTypeFontLoader(FileHandleResolver resolver) {
		super(resolver);
	}

	@Override
	public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, AssetLoaderParameters<FreeTypeFontGenerator> parameter) {
		return null;
	}

	@Override
	public FreeTypeFontGenerator load(AssetManager assetManager, String fileName, FileHandle file, AssetLoaderParameters<FreeTypeFontGenerator> parameter) {
		return new FreeTypeFontGenerator(Gdx.files.internal("fonts/acmesa.ttf"));
	}

}
