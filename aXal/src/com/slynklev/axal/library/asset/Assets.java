package com.slynklev.axal.library.asset;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;

public class Assets {
	private static final CustomAssetManager manager = new CustomAssetManager();
	
	private Assets(){}
	
	public static final <T> void	addAsset(java.lang.String fileName, java.lang.Class<T> type, T asset) {
		manager.addAsset(fileName, type, asset);
	}
	
	public static final void	clear() {
		manager.clear();
	}
	
	public static final <T> boolean	containsAsset(T asset) {
		return manager.containsAsset(asset);
	}
	
	public static final void	dispose() {
		manager.dispose();
	}
	
	public static final void	finishLoading() {
		manager.finishLoading();
	}
	
	public static final <T> T	get(AssetDescriptor<T> assetDescriptor) {
		return manager.get(assetDescriptor);
	}
	
	public static final <T> T	get(java.lang.String fileName)  {
		return manager.get(fileName);
	}
	
	public static final <T> T	get(java.lang.String fileName, java.lang.Class<T> type) {
		return manager.get(fileName, type);
	}
	
	public static final <T> java.lang.String	getAssetFileName(T asset) {
		return manager.getAssetFileName(asset);
	}
	
	public static final Array<java.lang.String>	getAssetNames() {
		return manager.getAssetNames();
	}
	
	public static final java.lang.Class	getAssetType(java.lang.String fileName) {
		return manager.getAssetType(fileName);
	}
	
	public static final Array<java.lang.String>	getDependencies(java.lang.String fileName) {
		return manager.getDependencies(fileName);
	}
	
	public static final java.lang.String	getDiagnostics() {
		return manager.getDiagnostics();
	}
	
	public static final int	getLoadedAssets() {
		return manager.getLoadedAssets();
	}
	
	public static final <T> AssetLoader	getLoader(java.lang.Class<T> type) {
		return manager.getLoader(type);
	}
	
	public static final <T> AssetLoader	getLoader(java.lang.Class<T> type, java.lang.String fileName) {
		return manager.getLoader(type, fileName);
	}
	
	public static final Logger	getLogger()  {
		return manager.getLogger();
	}
	
	public static final float	getProgress()  {
		return manager.getProgress();
	}
	
	public static final int	getQueuedAssets()  {
		return manager.getQueuedAssets();
	}
	
	public static final int	getReferenceCount(java.lang.String fileName) {
		return manager.getReferenceCount(fileName);
	}
	
	public static final boolean	isLoaded(java.lang.String fileName)  {
		return manager.isLoaded(fileName);
	}
	
	public static final boolean	isLoaded(java.lang.String fileName, java.lang.Class type)  {
		return manager.isLoaded(fileName, type);
	}
	
	public static final void	load(AssetDescriptor desc) {
		manager.load(desc);
	}
	
	public static final <T> void	load(java.lang.String fileName, java.lang.Class<T> type) {
		manager.load(fileName, type);
	}
	
	public static final <T> void	load(java.lang.String fileName, java.lang.Class<T> type, AssetLoaderParameters<T> parameter) {
		manager.load(fileName, type, parameter);
	}
	
	public static final void	setErrorListener(AssetErrorListener listener) {
		manager.setErrorListener(listener);
	}
	
	public static final <T,P extends AssetLoaderParameters<T>> void	setLoader(java.lang.Class<T> type, AssetLoader<T,P> loader) {
		manager.setLoader(type, loader);
	}
	
	public static final <T,P extends AssetLoaderParameters<T>> void setLoader(java.lang.Class<T> type, java.lang.String suffix, AssetLoader<T,P> loader) {
		manager.setLoader(type, suffix, loader);
	}

	public static final void setReferenceCount(java.lang.String fileName, int refCount) {
		manager.setReferenceCount(fileName, refCount);
	}

	public static final void unload(java.lang.String fileName) {
		manager.unload(fileName);
	}

	public static final boolean	update() {
		return manager.update();
	}

	public static final boolean	update(int millis) {
		return manager.update(millis);
	}
}
