package com.slynklev.axal.library.containers;

import java.util.HashMap;

public class DefaultHashMap<K,V> extends HashMap<K,V> {
	private static final long serialVersionUID = 8497202600080163879L;
	
	protected V defaultValue;
	  
	  public DefaultHashMap(V defaultValue) {
	    this.defaultValue = defaultValue;
	  }
	  
	  @Override
	  public V get(Object k) {
	    V v = super.get(k);
	    return ((v == null) && !this.containsKey(k)) ? this.defaultValue : v;
	  }
}