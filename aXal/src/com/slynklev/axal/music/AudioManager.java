package com.slynklev.axal.music;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.slynklev.axal.library.asset.Assets;


public class AudioManager
{	 
	private static final float SFX_VOLUME = 0.7f;
	private static final float BGM_VOLUME = 0.5f;
	
	private AudioManager() {}
	
	public static void load() {
		Assets.load("Music/BetusBlues.mp3", Music.class);
		Assets.load("Sounds/Step.wav", Sound.class);
		Assets.finishLoading();
	}
	
	public static void playBGM(String file) {
		if(Assets.isLoaded(file, Music.class)) {
			final Music bgm = Assets.get(file, Music.class);
			
			if(!bgm.isPlaying()) {
				bgm.setVolume(BGM_VOLUME);
				bgm.play();
				bgm.setLooping(true);
			}
		}
	}
	
	public static void playSFX(String file){
		if(Assets.isLoaded(file, Sound.class)) {
			final Sound sfx = Assets.get(file, Sound.class);
			
			sfx.stop();
			sfx.loop(SFX_VOLUME);
		}
			
	}
	
	public static void stopBGM(String file) {
		if(Assets.isLoaded(file, Music.class)) {
			Assets.get(file, Music.class).stop();
		}
	}
	
	public static void stopSFX(String file) {
		if(Assets.isLoaded(file, Sound.class)) {
			Assets.get(file, Sound.class).stop();
		}
	}
	
}
