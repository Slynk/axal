package com.slynklev.axal.controllers;

import java.util.HashMap;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.slynklev.axal.models.Block;
import com.slynklev.axal.models.Player;
import com.slynklev.axal.models.Block.CollisionType;
import com.slynklev.axal.models.Player.State;
import com.slynklev.axal.models.World;
import com.slynklev.axal.music.AudioManager;

public class PlayerController {
	enum Keys {
		LEFT, RIGHT, JUMP, FIRE
	}

	private static final long LONG_JUMP_PRESS 	= 150l;
	private static final float ACCELERATION 	= 20f;
	private static final float GRAVITY 			= -20f;
	private static final float MAX_JUMP_SPEED	= 7f;
	private static final float DAMP 			= 0.90f;
	private static final float MAX_VEL 			= 4f;
	
	// these are temporary
	private static final float WIDTH = 10f;

	private World 	world;
	private Player 	player;
	private long	jumpPressedTime;
	private boolean jumpingPressed;
	private boolean grounded = false;
	
	private Array<Block> collidable = new Array<Block>();
	
	// This is the rectangle pool used in collision detection
	// Good to avoid instantiation each frame
	private Pool<Rectangle> rectPool = new Pool<Rectangle>() {
		@Override
		protected Rectangle newObject() {
			return new Rectangle();
		}
	};

	static HashMap<Keys, Boolean> keys = new HashMap<PlayerController.Keys, Boolean>();
	static {
		keys.put(Keys.LEFT, false);
		keys.put(Keys.RIGHT, false);
		keys.put(Keys.JUMP, false);
		keys.put(Keys.FIRE, false);
	};

	public PlayerController(World world) {
		this.world = world;
		this.player = world.getPlayer();
	}

	// ** Key presses and touches **************** //

	public void leftPressed() {
		keys.get(keys.put(Keys.LEFT, true));
	}

	public void rightPressed() {
		keys.get(keys.put(Keys.RIGHT, true));
	}

	public void jumpPressed() {
		keys.get(keys.put(Keys.JUMP, true));
	}

	public void firePressed() {
		keys.get(keys.put(Keys.FIRE, false));
	}

	public void leftReleased() {
		keys.get(keys.put(Keys.LEFT, false));
	}

	public void rightReleased() {
		keys.get(keys.put(Keys.RIGHT, false));
	}

	public void jumpReleased() {
		keys.get(keys.put(Keys.JUMP, false));
		jumpingPressed = false;
	}

	public void fireReleased() {
		keys.get(keys.put(Keys.FIRE, false));
	}

	/** The main update method **/
	public void update(float delta) {
		processInput();
		if (grounded && player.getState().equals(State.JUMPING)) {
			player.setIdle();
			AudioManager.stopSFX("Sounds/Step.wav");
		}
		player.getAcceleration().y = GRAVITY;
		player.getAcceleration().mul(delta);
		player.getVelocity().add(player.getAcceleration().x, player.getAcceleration().y);
		checkCollisionWithBlocks(delta);
		player.getVelocity().x *= DAMP;
		if (player.getVelocity().x > MAX_VEL) {
			player.getVelocity().x = MAX_VEL;
		}
		if (player.getVelocity().x < -MAX_VEL) {
			player.getVelocity().x = -MAX_VEL;
		}
		player.update(delta);
		
		player.setApex(Math.max(player.getApex(), player.getPosition().y));
		
		if(player.getPosition().y < 0 || player.getPercentHealth() <= 0f)
			player.setState(State.DIEING);
	}

	/** Change Player's state and parameters based on input controls **/
	private boolean processInput() {
		if (keys.get(Keys.JUMP)) {
			if (player.getState().equals(State.IDLE) || player.getState().equals(State.RUNNING)) {
				jumpingPressed = true;
				grounded = false;
				jumpPressedTime = System.currentTimeMillis();
				player.setState(State.JUMPING);
				AudioManager.stopSFX("Sounds/Step.wav");
				player.getVelocity().y = MAX_JUMP_SPEED; 
			} else {
				if (jumpingPressed && ((System.currentTimeMillis() - jumpPressedTime) >= LONG_JUMP_PRESS)) {
					jumpingPressed = false;
				} else {
					if (jumpingPressed) {
						player.getVelocity().y = MAX_JUMP_SPEED;
					}
				}
			}
		}
		if (keys.get(Keys.LEFT)) {
			// left is pressed
			player.setFacingLeft(true);
			if (!player.getState().equals(State.JUMPING)) {
				player.setState(State.RUNNING);
				AudioManager.playSFX("Sounds/Step.wav");
			}
			player.getAcceleration().x = -ACCELERATION;
		} else if (keys.get(Keys.RIGHT)) {
			// left is pressed
			player.setFacingLeft(false);
			if (!player.getState().equals(State.JUMPING)) {
				player.setState(State.RUNNING);
				AudioManager.playSFX("Sounds/Step.wav");
			}
			player.getAcceleration().x = ACCELERATION;
		} else {
			if (!player.getState().equals(State.JUMPING)) {
				player.setIdle();
				AudioManager.stopSFX("Sounds/Step.wav");
			}
			player.getAcceleration().x = 0;
			
		}
		return false;
	}
	
	private void checkCollisionWithBlocks(float delta) {
		final boolean takeFallDamage;
		final float percent;
		if(player.getVelocity().y <-10){
			takeFallDamage=true&&!grounded;
			percent = player.getVelocity().y / Player.TERMINAL_VELOCITY;
		} else {
			takeFallDamage = false;
			percent = 0f;
		}
		
		player.getVelocity().mul(delta);
		Rectangle playerRect = rectPool.obtain();
		playerRect.set(player.getBounds().x, player.getBounds().y, player.getBounds().width, player.getBounds().height);
		
		int startX, endX, startY, endY;
		
		// Check top and bottom collision
		startX = (int) player.getBounds().x;
		endX = (int) (player.getBounds().x + player.getBounds().width);
		
		
		if (player.getVelocity().y < 0) {
		
			startY = endY = (int) Math.floor(player.getBounds().y + player.getVelocity().y);
		} else {
			startY = endY = (int) Math.floor(player.getBounds().y + player.getBounds().height + player.getVelocity().y);
		}
		populateCollidableBlocks(startX, startY, endX, endY);
		playerRect.y += player.getVelocity().y;
		for (Block block : collidable) {
			if (block == null) continue;
			if (playerRect.overlaps(block.getBounds())) {
				final Vector2 pC = new Vector2(), bC = new Vector2();
				
				playerRect.getCenter(pC);
				block.getBounds().getCenter(bC);
				
				if(Math.abs(pC.y - bC.y) > Math.abs(pC.x - bC.x)) {
					
					if(block.getCollisionType().equals(CollisionType.FULL) ||
					  (block.getCollisionType().equals(CollisionType.TOP) && (player.getState().equals(State.IDLE) || (player.getVelocity().y < 0 && player.getApex() >= block.getBounds().y + block.getBounds().height)))){
				
						if (player.getVelocity().y < 0) {
							grounded = true;
						}
						
						world.getCollisionRects().add(block.getBounds());
						
						player.getPosition().y = player.getVelocity().y > 0f ? block.getBounds().y - (player.getBounds().height) : block.getBounds().y + block.getBounds().height;
						
						player.getVelocity().y = 0;
							
						break;
					}
				}
			}
		}
		playerRect.y = player.getPosition().y;
		
		// Check left and right collision
		startY = (int) player.getBounds().y;
		endY = (int) (player.getBounds().y + player.getBounds().height);
		
		
		if (player.getVelocity().x < 0) {
			startX = endX = (int) Math.floor(player.getBounds().x + player.getVelocity().x);
		} else {
			startX = endX = (int) Math.floor(player.getBounds().x + player.getBounds().width + player.getVelocity().x);
		}
		populateCollidableBlocks(startX, startY, endX, endY);
		playerRect.x += player.getVelocity().x;
		world.getCollisionRects().clear();
		for (Block block : collidable) {
			if (block == null) continue;
			if (playerRect.overlaps(block.getBounds())) {
				
				final Vector2 pC = new Vector2(), bC = new Vector2();
				
				playerRect.getCenter(pC);
				block.getBounds().getCenter(bC);
				
				if(Math.abs(pC.x - bC.x) > Math.abs(pC.y - bC.y) && block.getCollisionType().equals(CollisionType.FULL)) {
				
					world.getCollisionRects().add(block.getBounds());
					
					player.getPosition().x = player.getVelocity().x > 0f ? block.getBounds().x - (player.getBounds().width) : block.getBounds().x + block.getBounds().width;
					
					player.getVelocity().x = 0;
					
					break;
				}
			}
		}
		playerRect.x = player.getPosition().x;
		
		// Apply
		player.getPosition().add(player.getVelocity());
		player.getBounds().x = player.getPosition().x;
		player.getBounds().y = player.getPosition().y;
		player.getVelocity().mul(1 / delta);
		
		if(takeFallDamage && grounded) {
			player.takeFallDamage(percent);
		}
	}

	private void populateCollidableBlocks(int startX, int startY, int endX, int endY) {
		collidable.clear();
		for (int x = startX; x <= endX; x++) {
			for (int y = startY; y <= endY; y++) {
				if (x >= 0 && x < world.getLevel().getWidth() && y >=0 && y < world.getLevel().getHeight()) {
					collidable.add(world.getLevel().get(x, y));
				}
			}
		}
	}
}
