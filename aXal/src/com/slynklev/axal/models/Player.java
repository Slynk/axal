package com.slynklev.axal.models;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Player {
	public enum State {
		
		IDLE, RUNNING, JUMPING, DIEING
	}
	
	public static final float SIZE = 0.5f; // half a unit
	public static final float SPEED = 2f;	// unit per second
	public static final float JUMP_VELOCITY = 1f;
	public static final float TERMINAL_VELOCITY = -10f; // Speed at which we take fall damage

	Rectangle 	bounds = new Rectangle();
	Vector2 	position = new Vector2();
	Vector2 	acceleration = new Vector2();
	Vector2 	velocity = new Vector2();
	float		apex = 0.0f;
	State		state = State.IDLE;
	boolean		facingLeft = true;
	
	float		stateTime = 0;
	int			maxHealth = 100;
	int			currentHealth = 100;
	

	public Player(final Vector2 position) {
		this.position = new Vector2(position);
		this.bounds.x = position.x;
		this.bounds.y = position.y;
		this.bounds.height = SIZE;
		this.bounds.width = SIZE;
	}
	
	
	
	public void reset(final Vector2 position) {
		this.position = new Vector2(position);
		this.bounds.x = position.x;
		this.bounds.y = position.y;
		stateTime = 0;
		state = State.IDLE;
		facingLeft = true;
		velocity.set(0f, 0f);
		acceleration.set(0f, 0f);
		currentHealth=100;
	}

	public Rectangle getBounds() {
		return bounds;
	}
	
	public Vector2 getPosition() {
		return position;
	}
	
	public void setState(State newState) {
		this.state = newState;
	}
	
	public void setIdle() {
		this.state = State.IDLE;
		this.apex = 0f;
		
		
		
	}

	public void update(float delta) {
		stateTime += delta;
		final Vector2 v = velocity.cpy();
		position.add(v.mul(delta));
	}

	public void setFacingLeft(boolean b) {
		facingLeft = b;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public Vector2 getAcceleration() {
		return acceleration;
	}

	public boolean isFacingLeft() {
		return facingLeft;
	}

	public State getState() {
		return state;
	}

	public float getStateTime() {
		return stateTime;
	}

	public void setPosition(Vector2 position) {
		this.position = new Vector2(position);
	}
	
	public void setApex(final float apex) {
		this.apex = apex;
	}
	
	public float getApex() {
		return apex;
	}
	
	public float getPercentHealth()
	{
		return (float)currentHealth/(float)maxHealth;
	}
	
	private void takeDamage(final int dmg) {
		currentHealth -= dmg;
		
		if(currentHealth < 0) {
			currentHealth = 0;
		}
	}

	public void takeFallDamage(final float percent) {
		takeDamage((int)(10f * percent));
	}
}
