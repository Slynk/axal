package com.slynklev.axal.models;

import java.util.ArrayList;
import java.util.List;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

public class World {
	/** Our player controlled hero **/
	Player player;
	
	/** A world has a level through which Bob needs to go through **/
	Level level;
	
	/** The collision boxes **/
	Array<Rectangle> collisionRects = new Array<Rectangle>();

	// Getters -----------
	public Player getPlayer() {
		return player;
	}
	public Array<Rectangle> getCollisionRects() {
		return collisionRects;
	}
	public Level getLevel() {
		return level;
	}
	/** Return only the blocks that need to be drawn **/
	public List<Block> getDrawableBlocks(int width, int height) {
		int x = (int)player.getPosition().x - width;
		int y = (int)player.getPosition().y - height;
		if (x < 0) {
			x = 0;
		}
		if (y < 0) {
			y = 0;
		}
		int x2 = x + 2 * width;
		int y2 = y + 2 * height;
		if (x2 >= level.getWidth()) {
			x2 = level.getWidth() - 1;
		}
		if (y2 >= level.getHeight()) {
			y2 = level.getHeight() - 1;
		}
		
		List<Block> blocks = new ArrayList<Block>();
		Block block;
		for (int col = x; col <= x2; col++) {
			for (int row = y; row <= y2; row++) {
				block = level.getBlocks()[col][row];
				if (block != null) {
					blocks.add(block);
				}
			}
		}
		return blocks;
	}

	// --------------------
	
	public World(final String levelFile) {
		loadLevel(levelFile);
	}
	
	private void loadLevel(final String file) {
		Input input = null;
		try {
			input = new Input(Gdx.files.internal("levels/" + file).read());
			level =  new Kryo().readObject(input, Level.class);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			input.close();
		}
		
		player = new Player(level.getSpawnLocation());
	}
	
	public void reset() {
		player.reset(level.getSpawnLocation());
	}
}
