package com.slynklev.axal.models;

import java.io.Serializable;
import java.util.ArrayList;

import com.slynklev.axal.models.components.Component;
import com.slynklev.axal.models.components.Component.Type;

public class Entity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Component> components = new ArrayList<Component>();
	private float x, y;
	
	public Entity(){
		this(0, 0);
	}
	
	public Entity(final Entity other) {
		this(other.x, other.y);
		
		components.addAll(other.components);
	}
	
	public Entity(final float x, final float y) {
		this.x = x;
		this.y = y;
	}
	
	public ArrayList<Component> getAllOfType(final Type t) {
		final ArrayList<Component> ret = new ArrayList<Component>();
		for(final Component c : components)
			if(c.isType(t))
				ret.add(c);
		
		return ret;
	}
	
	public void setComponents(final Component... components) {
		for(final Component c : components)
			this.components.add(c);
	}
	
	public void setPosition(final float x, final float y) {
		this.x = x;
		this.y = y;
	}
	
	public float x() {
		return x;
	}
	
	public float y() {
		return y;
	}
	
	
}
