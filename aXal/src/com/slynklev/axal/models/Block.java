package com.slynklev.axal.models;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Block {
	public static final float SIZE = 1f;

	int				texture;
	Vector2 		position;
	Rectangle 		bounds = new Rectangle();
	CollisionType 	collision;
	
	
	public enum CollisionType {
		NONE, TOP, FULL;
	}

	public Block() {}
	
	public Block(final Vector2 pos, final int texture) {
		this(pos, texture, CollisionType.FULL);
	}
	
	public Block(final Vector2 pos, final int texture, final CollisionType collision) {
		this.position = pos;
		this.bounds.setX(pos.x);
		this.bounds.setY(pos.y);
		this.bounds.width = SIZE;
		this.bounds.height = SIZE;
		this.texture = texture;
		this.collision = collision;
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public Vector2 getPosition() {
		return position;
	}
	
	public int getTexture() {
		return texture;
	}
	
	public CollisionType getCollisionType() {
		return collision;
	}
	
	public void setTexture(final int texture) {
		this.texture = texture;
	}
}
