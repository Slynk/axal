package com.slynklev.axal.models;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

public class Level {
	private Vector2 spawnLocation = new Vector2(0, 0);
	private int width, height;
	private Block[][] blocks;
	private ArrayList<Entity> entities = new ArrayList<Entity>();
	
	public Level() {}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Block[][] getBlocks() {
		return blocks;
	}

	public void setBlocks(Block[][] blocks) {
		this.blocks = blocks;
		width = blocks.length;
		height = blocks[0].length;
	}
	
	public Block get(int x, int y) {
		return blocks[x][y];
	}

	public Vector2 getSpawnLocation() {
		return spawnLocation;
	}

	public void setSpawnLocation(Vector2 spawnLocation) {
		this.spawnLocation = spawnLocation;
	}
	
	public ArrayList<Entity> getEntities() {
		return entities;
	}
	
	public void setEntities(final Entity...entities){
		for(final Entity e : entities)
			this.entities.add(e);
	}
}
