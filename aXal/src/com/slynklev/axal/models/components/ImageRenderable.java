package com.slynklev.axal.models.components;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.slynklev.axal.models.Entity;

public class ImageRenderable extends Renderable {
	private static final long serialVersionUID = 1L;

	protected TextureRegion region;
	
	public ImageRenderable(final Entity parent, final float width, final float height) {
		super(parent, width, height);
	}

	public void setRegion(final TextureRegion region) {
		this.region = region;
	}
	
	@Override
	public void render(final SpriteBatch batch) {
		if(region == null)
			return;
		
		batch.draw(region, parent.x(), parent.y(), width, height);
	}

}
