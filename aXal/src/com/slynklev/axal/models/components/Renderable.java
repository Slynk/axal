package com.slynklev.axal.models.components;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.slynklev.axal.models.Entity;

public abstract class Renderable extends Component {
	protected static final long serialVersionUID = 1L;
	
	protected float width, height;

	public Renderable(final Entity parent, final float width, final float height) {
		super(Type.RENDERABLE, parent);
		
		this.width = width;
		this.height = height;
	}

	public abstract void render(final SpriteBatch batch);
}
