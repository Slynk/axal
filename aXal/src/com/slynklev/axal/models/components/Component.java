package com.slynklev.axal.models.components;

import java.io.Serializable;

import com.slynklev.axal.models.Entity;

public abstract class Component implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected final Type type;
	protected final Entity parent;
	
	public Component(final Type type, final Entity parent){
		this.type = type;
		this.parent = parent;
	}
	
	public boolean isType(final Type t) {
		return type == t;
	}
	
	public Type getType() {
		return type;
	}
	
	public Entity getParent() {
		return parent;
	}
	
	public void unload() { }
	
	public enum Type {
		RENDERABLE,
		CONTROLLABLE,
		SCRIPTABLE,
		DESTROYABLE,
		COLLIDABLE,
	}
}
