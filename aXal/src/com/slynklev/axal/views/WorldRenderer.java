package com.slynklev.axal.views;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.slynklev.axal.aXal;
import com.slynklev.axal.library.asset.Assets;
import com.slynklev.axal.library.asset.FreeTypeFontLoader;
import com.slynklev.axal.models.Block;
import com.slynklev.axal.models.Entity;
import com.slynklev.axal.models.Player;
import com.slynklev.axal.models.Player.State;
import com.slynklev.axal.models.World;
import com.slynklev.axal.models.components.Component;
import com.slynklev.axal.models.components.Component.Type;
import com.slynklev.axal.models.components.ImageRenderable;
import com.slynklev.axal.models.components.Renderable;

public class WorldRenderer {
	private static final float RUNNING_FRAME_DURATION = 0.06f;
	
	private int viewHeight = 320;
	private float cameraWidth = 1f;
	
	private static final float cameraHeight = 10f;
	
	

	private World world;
	private OrthographicCamera cam;
	private Gui gui;

	/** for debug rendering **/
	ShapeRenderer debugRenderer = new ShapeRenderer();
	ShapeRenderer shapeRenderer = new ShapeRenderer();
	
	/** Textures **/
	private TextureRegion[] blockTextures;
	
	private TextureRegion playerIdleLeft;
	private TextureRegion playerIdleRight;
	private TextureRegion playerFrame;
	private TextureRegion playerJumpLeft;
	private TextureRegion playerFallLeft;
	private TextureRegion playerJumpRight;
	private TextureRegion playerFallRight;
	
	/** Animations **/
	private Animation walkLeftAnimation;
	private Animation walkRightAnimation;

	private SpriteBatch spriteBatch;
	private SpriteBatch debugBatch;

	public WorldRenderer(World world) {
		Assets.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontLoader(new InternalFileHandleResolver()));
		
		
		this.world = world;
		this.cam = new OrthographicCamera(cameraWidth, cameraHeight);
		this.cam.position.set(cameraWidth / 2f, cameraHeight / 2f, 0);
		this.cam.update();
		spriteBatch = new SpriteBatch();
		debugBatch = new SpriteBatch();
		loadTextures();
		gui = new Gui(debugBatch, shapeRenderer, "fonts/debug");
		
		
		final Vector2 position = world.getLevel().getSpawnLocation();
		final Entity testEntity = new Entity(position.x, position.y);
		final ImageRenderable testRenderable = new ImageRenderable(testEntity, Player.SIZE, Player.SIZE);
		testRenderable.setRegion(playerJumpLeft);
		testEntity.setComponents(testRenderable);
		
		final Entity testEntity2 = new Entity(15f, 4f);
		final ImageRenderable testRenderable2 = new ImageRenderable(testEntity2, Player.SIZE, Player.SIZE);
		testRenderable2.setRegion(playerJumpLeft);
		testEntity2.setComponents(testRenderable2);
		
		
		world.getLevel().setEntities(testEntity, testEntity2);
	}
	
	public void resize (int width, int height) {
		cameraWidth = (width * cameraHeight /height);
		
		this.cam = new OrthographicCamera(cameraWidth, cameraHeight);
		this.cam.update();
	}
	
	private void loadTextures() {
		/**
		 * Load Character
		 */
		final TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("images/atlas/textures.pack"));
		Assets.addAsset("images/packs/textures", TextureAtlas.class, atlas);
		playerIdleLeft = atlas.findRegion("player-01");
		playerIdleRight = new TextureRegion(playerIdleLeft);
		playerIdleRight.flip(true, false);
		playerJumpLeft = atlas.findRegion("player-up");
		playerJumpRight = new TextureRegion(playerJumpLeft);
		playerJumpRight.flip(true, false);
		playerFallLeft = atlas.findRegion("player-down");
		playerFallRight = new TextureRegion(playerFallLeft);
		playerFallRight.flip(true, false);
		TextureRegion[] walkLeftFrames = new TextureRegion[5];
		for (int i = 0; i < 5; i++) {
			walkLeftFrames[i] = atlas.findRegion("player-0" + (i + 2));
		}
		walkLeftAnimation = new Animation(RUNNING_FRAME_DURATION, walkLeftFrames);

		TextureRegion[] walkRightFrames = new TextureRegion[5];

		for (int i = 0; i < 5; i++) {
			walkRightFrames[i] = new TextureRegion(walkLeftFrames[i]);
			walkRightFrames[i].flip(true, false);
		}
		walkRightAnimation = new Animation(RUNNING_FRAME_DURATION, walkRightFrames);
		
		/**
		 * Load Tiles
		 */
		
		final TextureAtlas tileAtlas = new TextureAtlas(Gdx.files.internal("images/packs/tiles.pack"));
		Assets.addAsset("images/packs/tiles", TextureAtlas.class, tileAtlas);
		blockTextures = new TextureRegion[tileAtlas.getRegions().size];
		
		for(int i = 0; i < blockTextures.length; i++) {
			blockTextures[i] = tileAtlas.findRegion("" + i);
		}
		
		/**
		 * Load Fonts
		 */
		Assets.load("fonts/acmesa.ttf", FreeTypeFontGenerator.class);
		Assets.finishLoading();
		FreeTypeFontGenerator gen = Assets.get("fonts/acmesa.ttf", FreeTypeFontGenerator.class);
		final BitmapFont debugFont = gen.generateFont(10);
		debugFont.setColor(Color.WHITE);
		Assets.addAsset("fonts/debug", BitmapFont.class, debugFont);
		gen.dispose();
	}


	public void render(float delta) {
		setCamera();
		spriteBatch.begin();
			drawBlocks();
			drawRenderables();
			drawPlayer();
		spriteBatch.end();
		drawCollisionBlocks();
		if (aXal.DEBUG)
			drawDebug();
		
		drawGui(delta);
	}
	
	public void dispose() {
		spriteBatch.dispose();
		debugBatch.dispose();
		Assets.dispose();
	}
	
	private void setCamera() {
		final Player player = world.getPlayer();
		cam.position.set(player.getPosition().x, player.getPosition().y, 0);
		cam.update();
		spriteBatch.setProjectionMatrix(cam.combined);
		debugRenderer.setProjectionMatrix(cam.combined);
	}

	private void drawBlocks() {
		for (Block block : world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight)) {
			spriteBatch.draw(blockTextures[block.getTexture()], block.getPosition().x , block.getPosition().y , Block.SIZE , Block.SIZE );
		}
	}
	
	private void drawCollisionBlocks() {
		debugRenderer.begin(ShapeType.Filled);
		debugRenderer.setColor(new Color(1, 1, 1, 1));
		for (Rectangle rect : world.getCollisionRects()) {
			debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}
		debugRenderer.end();
	}

	private void drawPlayer() {
		Player player = world.getPlayer();
		playerFrame = player.isFacingLeft() ? playerIdleLeft : playerIdleRight;
		if(player.getState().equals(State.RUNNING)) {
			playerFrame = player.isFacingLeft() ? walkLeftAnimation.getKeyFrame(player.getStateTime(), true) : walkRightAnimation.getKeyFrame(player.getStateTime(), true);
		} else if (player.getState().equals(State.JUMPING)) {
			if (player.getVelocity().y > 0) {
				playerFrame = player.isFacingLeft() ? playerJumpLeft : playerJumpRight;
			} else {
				playerFrame = player.isFacingLeft() ? playerFallLeft : playerFallRight;
			}
		}
		
		spriteBatch.draw(playerFrame, player.getPosition().x, player.getPosition().y, Player.SIZE, Player.SIZE);
	}
	
	private void drawGui(float delta)
	{
		gui.setHealth(world.getPlayer().getPercentHealth());
		gui.render(delta);
	}
	

	private void drawDebug() {
		// render blocks
		debugRenderer.begin(ShapeType.Line);
		for (Block block : world.getDrawableBlocks((int)cameraWidth, (int)cameraHeight)) {
			Rectangle rect = block.getBounds();
			debugRenderer.setColor(new Color(1, 0, 0, 1));
			debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}
		// render Bob
		Player player = world.getPlayer();
		Rectangle rect = player.getBounds();
		debugRenderer.setColor(new Color(0, 1, 0, 1));
		debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		debugRenderer.end();
	}
	
	private void drawRenderables() {
		final ArrayList<Entity> entities = world.getLevel().getEntities();
		
		for(final Entity e : entities) {
			final ArrayList<Component> renderables = e.getAllOfType(Type.RENDERABLE);
			
			for(final Component renderable : renderables) {
				((Renderable) renderable).render(spriteBatch);
			}
		}
	}
}
