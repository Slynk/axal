package com.slynklev.axal.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.slynklev.axal.library.asset.Assets;

public class Gui {
	/** for FPS **/
	private static final float FPS_WEIGHT = .1f;
	private static final long TIME_TO_UPDATE = 500000000;
	private float fps = 1000000000f / 60f;
	private float toDrawFps = 1000000000f / 60f;
	private long time = 1;
	private long lastUpdate = 1000000000;
	
	private Rectangle healthBar = new Rectangle();
	
	private float health = 1f;
	
	/* For Rendering */
	private final SpriteBatch staticBatch;
	private final ShapeRenderer shapes;
	private final BitmapFont font;
	
	public Gui(final SpriteBatch staticBatch, final ShapeRenderer shapes, final String font) {
		this.staticBatch = staticBatch;
		this.shapes = shapes;
		this.font = Assets.get(font, BitmapFont.class);
		
		// Define healthbar dimensions based on a percent of screen width and height
		healthBar.x = 0.01f;
		healthBar.y = .89f;
		healthBar.width = 0.25f;
		healthBar.height = 0.1f;
	}
	
	public void show() {
		
	}
	
	public void render(float delta) {
		final int w = Gdx.graphics.getWidth();
		final int h = Gdx.graphics.getHeight();
		
		/** Render Healthbar **/
		shapes.begin(ShapeType.Filled);
		shapes.setColor(0, 0.3f, 0, 1);
		shapes.rect(w * healthBar.x, h * healthBar.y, w * healthBar.width * health, h * healthBar.height);
		shapes.end();
		
		shapes.begin(ShapeType.Line);
		shapes.setColor(1, 1, 1, 1);
		shapes.rect(w * healthBar.x, h * healthBar.y, w * healthBar.width, h * healthBar.height);
		shapes.end();
		
		/** Render FPS counter **/
		final String toPrint = "FPS: " + (((float)((int)((1000000000 / toDrawFps) * 100)))/100f);
		
		staticBatch.begin();
		font.drawMultiLine(staticBatch,  toPrint, w - font.getBounds(toPrint).width, h);
		staticBatch.end();
		
		fps = ((System.nanoTime() - time) * (1.0f - FPS_WEIGHT)) + (fps * FPS_WEIGHT);
		
		lastUpdate += System.nanoTime() - time;
		if(lastUpdate >= TIME_TO_UPDATE) {
			lastUpdate = 0;
			toDrawFps = fps;
		}
		
		time = System.nanoTime();
	}
	
	public void dispose() {
		
	}
	
	public void setHealth(final float health) {
		this.health = health;
	}
}
