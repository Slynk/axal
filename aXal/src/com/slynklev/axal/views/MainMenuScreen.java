package com.slynklev.axal.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.slynklev.axal.aXal;

public class MainMenuScreen implements Screen {
	private final Stage stage;
	private Skin skin;
	private TextButton.TextButtonStyle tbs;
	
	public MainMenuScreen() {
		loadSkin();
		
		stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        final Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);
        
        final TextButton btnPlay = new TextButton("Play", tbs);
        btnPlay.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				aXal.game.addScreen(new GameScreen());
			}
        });
        
        
        table.add(btnPlay);
	}
	
	private void loadSkin(){
		skin = new Skin(Gdx.files.internal("data/uiMainMenu_skin.json"));
		tbs = new TextButton.TextButtonStyle(skin.get(TextButtonStyle.class));
		
		
		final FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/acmesa.ttf"));
		final BitmapFont font = gen.generateFont(10);
		font.setColor(Color.WHITE);
		gen.dispose();
		
		final NinePatch button_9 = new NinePatch(new Texture(Gdx.files.internal("images/ui/button.9.png")));
		
		tbs.font = font;
		tbs.up = new NinePatchDrawable(button_9);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

        Table.drawDebug(stage); // This is optional, but enables debug lines for tables.
	}

	@Override
	public void resize(int width, int height) {
		stage.setViewport(width, height, true);
	}

	@Override
	public void show() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
