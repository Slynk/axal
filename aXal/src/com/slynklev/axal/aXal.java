package com.slynklev.axal;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.slynklev.axal.library.containers.DefaultHashMap;
import com.slynklev.axal.views.GameScreen;
import com.slynklev.axal.views.MainMenuScreen;

public class aXal extends Game {
	public static final boolean DEBUG = true;
	
	public static aXal game;
	private final static DefaultHashMap<String, Flag> flags = new DefaultHashMap<String, Flag>(Flag.UNKNOWN);
	
	private final String[] args;
	private final ArrayList<Screen> screens = new ArrayList<Screen>();
	
	private boolean OVERRIDE_SCREEN = false;
	
	
	
	
	private enum Flag {
		LEVEL,
		UNKNOWN
	}
	
	public aXal(){
		this(new String[]{});
	}
	
	public aXal(final String[] args) {
		this.args = args;
		
		flags.put("-l", Flag.LEVEL);
		flags.put("-level", Flag.LEVEL);
		
		
	}
	
	@Override
	public void create() {
		if(interpretArgs(args)) {
			game = this;
			if(!OVERRIDE_SCREEN) {
				addScreen(new MainMenuScreen());
			}
		} else {
			System.exit(-1);
		}
	}
	
	public void addScreen(final Screen s) {
		screens.add(s);
		setScreen(s);
	}
	
	public void removeScreen() {
		final Screen s = screens.remove(screens.size()-1);
		
		setScreen(screens.get(screens.size()-1));
		
		s.dispose();
	}
	
	
	
	private final boolean interpretArgs(final String[] args) {
		boolean canInterpret = true;
		for(int i = 0; i < args.length; i++) {
			final Flag flag = flags.get(args[i]);
			
			switch (flag) {
			case LEVEL:
				if(i + 1 >= args.length) {
					canInterpret = false;
				} else {
					OVERRIDE_SCREEN = true;
					addScreen(new GameScreen(args[i+1]));
					i++;
				}
				break;
			default :
				canInterpret = false;
				break;
			}
		}
		
		return canInterpret;
	}
	
	public static void main(String[] args) {
		new LwjglApplication(new aXal(args), "aXal", 1280, 720, true);
	}
}
