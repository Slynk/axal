package com.slynklev.axal;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "aXal";
		cfg.useGL20 = false;
		cfg.width = 480;
		cfg.height = 320;
		
		for(final String s : args)
			System.out.println("Arg: " + s);
		
		new LwjglApplication(new aXal(args), cfg);
	}
}
